package dimon.java.HomeWork4;

import dimon.java.HomeWork4.custom.Bank;
import dimon.java.HomeWork4.custom.Vault;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String[] banks = {"PB", "OB", "PUMB"};
        String[] countries = {"USD", "EUR", "RUR"};
        float[][] courses = {{24.5f, 24.8f, 25f}, {26f, 30.1f, 31.5f}, {0.457f, 0.46f, 0.45f}};

        Vault[] vaults = new Vault[banks.length];

        for (int i = 0; i < vaults.length; i++) {
            vaults[i] = new Vault();
            vaults[i].vaultName = countries[i];
            vaults[i].banks = new Bank[banks.length];
            for (int j = 0; j < banks.length; j++) {
                Bank bank = new Bank();
                bank.bankName = banks[j];
                bank.course = courses[i][j];
                vaults[i].banks[j] = bank;
            }
        }

        for (Vault vault : vaults) {
            System.out.println(vault.vaultName);
            for (Bank bank : vault.banks) {
                System.out.println("\t" + bank.bankName);
                System.out.println("\t" + bank.course);
            }
        }

        Scanner in = new Scanner(System.in);
        System.out.println("Enter vault, please:");
        String vaultForSearch = in.nextLine();
        System.out.println("Enter bank, please:");
        String bankForSearch = in.nextLine();
        System.out.println("Enter how much money    , please:");
        int howMuchMoney = Integer.parseInt(in.nextLine());

        for (Vault vault : vaults) {
            if (vault.vaultName.equalsIgnoreCase(vaultForSearch)) {
                for (Bank bank : vault.banks) {
                    if (bank.bankName.equalsIgnoreCase(bankForSearch)) {
                        float result = howMuchMoney * bank.course;
                        System.out.println(vault.vaultName + " " + bank.bankName + " " + bank.course);
                        System.out.println(result);
                    }
                }
            }
        }

    }
}
